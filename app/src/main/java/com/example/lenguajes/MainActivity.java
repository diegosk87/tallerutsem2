package com.example.lenguajes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button b1;
    private Button b2;
    private Button b3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = findViewById(R.id.b1);
        b2 = findViewById(R.id.b2);
        b3 = findViewById(R.id.b3);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b1:
                Toast.makeText(this, getText(R.string.animal1), Toast.LENGTH_SHORT).show();
                break;
            case R.id.b2:
                Toast.makeText(this, getText(R.string.animal2), Toast.LENGTH_SHORT).show();
            break;
            case R.id.b3:
                Toast.makeText(this, getText(R.string.animal3), Toast.LENGTH_SHORT).show();
            break;
        }
    }
}
